---
title: "FIXME: Vortragstitelplatzhalter"
subtitle: "FIXME: Fancy Subtitle"
author: Vorname Nachname \url{<vorname.nachname@credativ.de>}
date: "FIXME: Datum an dem der Talk stattfindet"
institute: credativ GmbH
---

# Introduction - Basics

Items
=====

* First Item

* Second Item

	* First Sub-item

	* Second Sub-item

* Third Item

* Fourth Item

# Details

Example Unveil - Invisible
==========================

> - Item 1
> - Item 2
> - Item 3
> - Item 4

Boxes
=====

Normal Block
------------
General purpose block for arbitrary text.

Example Block
-------------
Block for example, like e.g. sample command invocations.

**Warning Block**
-----------------
Warning block for important messages, such as:
\
**Do not put too many boxes on one page!** `;-)`

Columns
=======

\columnsbegin
\column{0.6\textwidth}
Hier stehender Text schwebt frei neben einer Box. Anstatt einer Box oder
eines freien Texts kann hier auch ein Bild oder ein anderes Objekt
stehen.

\column{0.4\textwidth}
Blocks in Columns sind nicht supported

* First Item
* Second Item

\columnsend

\ 
==

\begin{center}
\Huge Danke für die Aufmerksamkeit
\end{center}

\backupbegin

Appendix
========

These frames are not counted

\backupend
